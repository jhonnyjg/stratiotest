package com.jhonny.es;

import java.text.ParseException;
import java.time.LocalTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Calculator {

    private final static Logger LOGGER = Logger.getLogger("com.jhonny.es.Calculator");

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter bill log: ");
            Integer result = calculatePrice(scanner.nextLine());

            LOGGER.log(Level.INFO, "** Result: " + result);

        } catch(Exception ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
    }


    private static Integer calculatePrice(String calls) throws ParseException {
        Map<String, LocalTime> stringStringMap = groupCallsByNumber(Arrays.asList(calls.split("\\\\n")));
        return calculateTotalPrice(stringStringMap);
    }

    private static Map<String, LocalTime> groupCallsByNumber(List<String> callList) throws ParseException {
        Map<String, LocalTime> map = new HashMap();
        Collections.sort(callList);

        for (String call : callList) {
            String[] callDetails = call.split(",");

            if (map.containsKey(callDetails[1])) {
                map.put(callDetails[1], calculateTime(map.get(callDetails[1]), callDetails[0]));
            } else {
                map.put(callDetails[1], LocalTime.parse(callDetails[0]));
            }
        }
        return map;
    }

    private static LocalTime calculateTime(LocalTime time1, String time2) throws ParseException {
        LocalTime d2 = LocalTime.parse(time2);
        return time1.plusHours(d2.getHour()).plusMinutes(d2.getMinute()).plusSeconds(d2.getSecond());

    }

    private static Integer calculateTotalPrice(Map<String, LocalTime> groupCalls) {
        Integer total = 0;

        removeLongestCall(groupCalls);

        for (LocalTime value : groupCalls.values()) {
            if (value.getMinute() >= 5) {
                total += value.getMinute() * 150;

                if (value.getSecond() > 0) {
                    total += 150;
                }

            } else if (value.getMinute() < 5) {
                total += value.getSecond() * 3;
            }
        }
        return total;
    }

    private static void removeLongestCall(Map<String, LocalTime> groupCalls) {
        Optional<LocalTime> first = groupCalls.values().stream().sorted(Comparator.reverseOrder()).findFirst();
        if (first.isPresent()) {
            groupCalls.values().remove(first.get());
        }
    }
}
